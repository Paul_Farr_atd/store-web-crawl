# README #

### Overview ###

These are the basic instructions for use of the web crawl program, the application will provide the most up to date instructions.

1. Database Data

The following SQL would need to be run in PHPmyAdmin or similar:

SELECT DISTINCT t0.id, t0.name, t1.web_site, t2.groups_id, t3.name FROM tl_store t0
LEFT JOIN tl_store_setting t1 ON ( t1.store_id = t0.id)
LEFT JOIN tl_group_stores t2 ON ( t2.store_id = t0.id)
LEFT JOIN tl_groups t3 ON ( t3.id = t2.groups_id )
WHERE t0.delete_at IS NULL AND t1.web_site is not null AND t0.enabled=1 ORDER BY t3.name,t0.name
INTO OUTFILE 'tl_store_custom.csv' FIELDS ENCLOSED BY '"' TERMINATED BY ',' LINES TERMINATED BY '\r\n'

It's important to note that the "outfile" parameter should match a folder in your local system environment.

2. Download the program from BitBucket.

3. Using the Webcrawl program will overtake control of the computer system, for an undetermined amount of time.

4. Results folder will add new information but not overwrite, erase the contents of the results folder to gather new results.

5. Report reflects the summary result of sites by group, identifying the site count per Group.
(note - does not identify the "reseller" option recently introduced)

6. Have the most up to date virus protection.

7. Please report any errors or exceptions.


### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact